import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateCommentDto } from "./dto/create-comment.dto";
import { GetCommentQueryDto } from "./dto/get-comment-query.dto";
import { CommentEntity } from "./entities/comment.entity";

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(CommentEntity)
    private commentRepository: Repository<CommentEntity>,
  ) {}

  async createComment(createCommentDto: CreateCommentDto) {
    const comment = this.commentRepository.create(createCommentDto);
    return this.commentRepository.save(comment);
  }

  async getComments(getCommentQueryDto: GetCommentQueryDto) {
    const comments = await this.commentRepository.find({
      skip: getCommentQueryDto.offset,
      take: getCommentQueryDto.limit,
      order: {
        date: "ASC",
      },
    });
    return comments;
  }
}
