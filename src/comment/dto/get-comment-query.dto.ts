import { ApiProperty } from "@nestjs/swagger";
import { IsNumber, IsOptional, Max, Min } from "class-validator";

export class GetCommentQueryDto {
  @ApiProperty({
    description:
      "Determines how many comments.entity will return. Range: [1, 10]",
  })
  @IsNumber()
  @Min(1)
  @Max(10)
  limit: number;

  @ApiProperty({
    description:
      "Determines how many comments.entity will be neglected. Range: [0, +inf)",
    required: false,
  })
  @IsNumber()
  @IsOptional()
  @Min(0)
  offset: number = 0;
}
