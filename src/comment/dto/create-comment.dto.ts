import { IsDate, IsString } from "class-validator";

export class CreateCommentDto {
  @IsString()
  author: string;

  @IsDate()
  date: Date;

  @IsString()
  content: string;
}
