import { Injectable } from "@nestjs/common";
import * as fs from "fs-extra";
import * as yaml from "js-yaml";

interface DatabaseConfig {
  PASSWORD: string;
  PORT: number;
}

interface Config {
  database: DatabaseConfig;
}

@Injectable()
export class ConfigService {
  constructor() {
    this.config = yaml.load(fs.readFileSync("./config.yml"));
    console.log(this.config);
  }
  config: Config;
}
